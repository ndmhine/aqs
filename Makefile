nbs = AQS.ipynb

py = $(nbs:%.ipynb=%.py)

doctxt = $(nbs:%.ipynb=%.txt)

all: $(py)

doc: $(doctxt)
	cat $(doctxt) > documentation.txt

%.py: %.ipynb
	jupyter-nbconvert --to script $<;

%.txt: %.py
	python $< -h > $@

